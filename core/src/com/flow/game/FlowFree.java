package com.flow.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

public class FlowFree extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

	int widht;
	int height;

	Stage stage;
	OrthographicCamera camera;
	Viewport viewport;

	Tablero t;

	float mouseX;
	float mouseY;


	//Start, lo que carga el programa al iniciar (una sola vez)
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");

		//widht = Gdx.graphics.getWidth();
		//height = Gdx.graphics.getHeight();

		stage = new Stage();
/*
		camera = new OrthographicCamera();
		viewport = new FitViewport(100.f, 100.f, camera);
		viewport.setScreenBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
*/
		//stage.setViewport(viewport);

		t = new Tablero();
		t.vecindad();
		//t.setTouchable(Touchable.enabled);
		t.addActors(stage);

		//t.info();

		Gdx.input.setInputProcessor(stage);


	}
/*
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width,height);
	}
*/

	@Override
	public void render () {
		mouseX = Gdx.input.getX();
		mouseY = Gdx.input.getY();
		/*
		viewport.apply();
		stage.setViewport(viewport);
		*/

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//camera.update();
		//t.tocar(Gdx.input.getX(), Gdx.input.getY());
		stage.draw();
		//t.draw();

	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
		stage.dispose();
	}
/*
	@Override
	public void resize(int width, int height) {
		viewport.update(width, height, true);
	}
	*/
}