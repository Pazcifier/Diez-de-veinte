package com.flow.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.util.ArrayList;


public class Tablero extends Cell {

    private int size;
    private ArrayList<Cell> tablero = new ArrayList<Cell>();
    ShapeRenderer shapeRenderer = new ShapeRenderer();


    public Tablero() {
        int x = 0;
        int y = 0;
        int cont = 0;

        FileHandle f = Gdx.files.internal("board.json");
        JsonReader o = new JsonReader(f.reader());
        try {
            o.beginObject();
            while (o.hasNext()) {
                String name = o.nextName();
                if (name.equals("dimension")) {
                    this.size = Integer.parseInt(o.nextString());
                    y = 90 * (this.size - 1);
                } else if (name.equals("tablero")) {
                    o.beginArray();
                    while (o.hasNext()) {
                        o.beginArray();
                        while (o.hasNext()) {
                            int color = o.nextInt();
                            if (color == 0) {
                                tablero.add(new Cell(x, y, false, color));
                            } else {
                                tablero.add(new Cell(x, y, true, color));
                            }
                            x += 90;
                            cont++;
                            if (cont == this.size) {
                                cont = 0;
                                x = 0;
                                y -= 90;
                            }
                        }
                        o.endArray();
                    }
                    o.endArray();
                } else {
                    o.skipValue();
                }

            }
            o.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void addActors(Stage stage) {
        for (Cell c : tablero) {
            stage.addActor(c);
        }
    }

    public void vecindad() {
        for (int k = 0; k < this.size * this.size; k++) {
//Arriba
            if (k < this.size) {
                tablero.get(k).vecindad.put("1", null);
            } else {
                tablero.get(k).vecindad.put("1", tablero.get(k - this.size));
            }
//Derecha
            if ((k + 1) % this.size == 0) {
                tablero.get(k).vecindad.put("2", null);
            } else {
                tablero.get(k).vecindad.put("2", tablero.get(k + 1));
            }
//Abajo
            if (k >= this.size*this.size - this.size) {
                tablero.get(k).vecindad.put("3", null);
            } else {
                tablero.get(k).vecindad.put("3", tablero.get(k + this.size));
            }
//Izquierda
            if ((k % this.size == 0) || (k == 0)) {
                tablero.get(k).vecindad.put("4", null);
            } else {
                tablero.get(k).vecindad.put("4", tablero.get(k - 1));
            }

        }
    }

/*
    public void draw() {
        int cont = 0;
        for (Cell c : tablero) {
            c.draw();
            System.out.println("Cell N°" + cont + " X: " + c.getX() + " Y: " + c.getY() + " Dimensiones: " + c.getWidth() + "x" + c.getHeight());
            cont++;
        }
    }
*/

    public void info() {
        int cont = 0;
        for (Cell c : tablero) {
            System.out.println("Nodo N°" + cont + " X: " + c.getX() + " Y: " + c.getY() + " getRight = " + c.getRight() + " getTop = " + c.getTop());
            cont++;
        }
    }

    public void tocar(int posx, int posy) {
        for (Cell c : tablero) {
            if ((posx >= c.getX()) && (posx <= c.getRight()) && (posy >= c.getY()) && (posy <= c.getTop()) && (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) && (c.getNodo())) {
                System.out.println("TOQUE");
            }
        }
    }
/*
    public void draw() {
        for (Cell c : tablero) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.WHITE);
            shapeRenderer.box(c.getX(), c.getY(), 0, c.getWidth(), c.getHeight(), 0);
            shapeRenderer.end();
            if (c.getNodo()) {
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                switch (c.getNodo()) {
                    case 1: {
                        shapeRenderer.setColor(Color.RED);
                        break;
                    }
                    case 2: {
                        shapeRenderer.setColor(Color.BLUE);
                        break;
                    }
                    case 3: {
                        shapeRenderer.setColor(Color.GREEN);
                        break;
                    }
                    case 4: {
                        shapeRenderer.setColor(Color.YELLOW);
                        break;
                    }
                }
                shapeRenderer.circle(c.getX() + 45, c.getY() + 45, 35);
                shapeRenderer.end();
            }
        }
    }
    */
}