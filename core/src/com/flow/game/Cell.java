package com.flow.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import java.util.HashMap;


public class Cell extends Actor {
    private int dim = 90;
    private boolean isCamino = false;
    private boolean isNodo = false;
    private Cell sucesor = null;
    private Cell predecesor = null;
    private int posicion;
    HashMap<String, Cell> vecindad = new HashMap<String, Cell>();
    //private int color = 0;
    private ShapeRenderer shapeRenderer = new ShapeRenderer();
    private int nodo = 0;

    private boolean dibujando = false;

    // private Color colorCamino = Color.WHITE;

    public Cell() {
    }

    public Cell(int posX, int posY, boolean nodo, int color) {
        this.setBounds(posX, posY, dim, dim);
        this.nodo = color;
        this.isNodo = nodo;
        this.setTouchable(Touchable.enabled);

        this.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (button == Input.Buttons.LEFT) {
                    if (isNodo) {
                        dibujando = true;
                    }
                    if (isCamino) {
                        isCamino = false;
                    }
                }
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dibujando = false;
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {

                if (dibujando) {
                    if ((vecindad.get("1") != null) && (y >= dim)) {
                        conectar(vecindad.get("1"));
                    }

                    if ((vecindad.get("2") != null) && (x >= dim)) {
                        conectar(vecindad.get("2"));
                    }

                    if ((vecindad.get("3") != null) && (y <= 0)) {
                        conectar(vecindad.get("3"));
                    }

                    if ((vecindad.get("4") != null) && (x <= 0)) {
                        conectar(vecindad.get("4"));

                    }
                }
            }
        });
    }

    public void conectar(Cell objetivo) {
        if (!objetivo.isNodo) {
            this.sucesor = objetivo;
            objetivo.conectarPredecesor(this);
            objetivo.nodo = this.nodo;
            this.isCamino = true;
        }
    }

    public void conectarPredecesor(Cell cell) {
        this.predecesor = cell;
        this.isCamino = true;
        this.posicion = cell.getPosicion() + 1;
    }

    public int getPosicion() {
        return this.posicion;
    }


    public boolean getNodo() {
        return isNodo;
    }

    public boolean getCamino() {
        return isCamino;
    }

    public void setCamino(boolean b) {
        isCamino = b;
    }
/*
    public int getCColor() {
        return this.color;
    }
*/


    @Override
    public void draw(Batch batch, float parentAlpha) {

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.WHITE);
        shapeRenderer.box(this.getX(), this.getY(), 0, this.getWidth(), this.getHeight(), 0);
        shapeRenderer.end();

        if (isNodo) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            switch (this.nodo) {
                case 1: {
                    shapeRenderer.setColor(Color.RED);
                    break;
                }
                case 2: {
                    shapeRenderer.setColor(Color.BLUE);
                    break;
                }
                case 3: {
                    shapeRenderer.setColor(Color.GREEN);
                    break;
                }
                case 4: {
                    shapeRenderer.setColor(Color.YELLOW);
                    break;
                }
                case 5: {
                    shapeRenderer.setColor(Color.ORANGE);
                    break;
                }
                case 6: {
                    shapeRenderer.setColor(Color.BROWN);
                    break;
                }
                case 7: {
                    shapeRenderer.setColor(Color.PINK);
                }
            }
            shapeRenderer.circle(this.getX() + (dim / 2), this.getY() + (dim / 2), 35);
            shapeRenderer.end();
        }

        if (isCamino) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            switch (this.nodo) {
                case 1: {
                    shapeRenderer.setColor(Color.RED);
                    break;
                }
                case 2: {
                    shapeRenderer.setColor(Color.BLUE);
                    break;
                }
                case 3: {
                    shapeRenderer.setColor(Color.GREEN);
                    break;
                }
                case 4: {
                    shapeRenderer.setColor(Color.YELLOW);
                    break;
                }
                case 5: {
                    shapeRenderer.setColor(Color.ORANGE);
                    break;
                }
                case 6: {
                    shapeRenderer.setColor(Color.BROWN);
                    break;
                }
                case 7: {
                    shapeRenderer.setColor(Color.PINK);
                }
            }
            shapeRenderer.box(this.getX() + dim / 4, this.getY() + dim/4, 0, dim / 2, dim / 2, 0);
            shapeRenderer.end();
        }
/*
        if (isCamino) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(setColorCamino(nodo));
            shapeRenderer.box(this.getX() + dim / 4, this.getY() + dim / 4, 0, dim / 2, dim / 2, 0);
            //shapeRenderer.setColor(Color.WHITE);
            shapeRenderer.end();
        }
        */
    }
}